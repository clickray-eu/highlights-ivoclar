'use strict';

// file: forms.js
// waitForLoad(".form, .widget-type-form, .widget-type-blog_content", "form", select);
// waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", function() {
//     $('form select').on('change', function() {
//         $('form .dropdown-header').addClass('dropdown-selected');
//     });
// });

// Functions for dropdown
function select($wrapper, $form) {
  $wrapper.find("select").each(function () {
    var parent = $(this).parent();
    var firstLabel = $(this).find('option').first().html();
    parent.addClass('dropdown_select');
    parent.append('<div class="dropdown-header"><span class="first-label">' + firstLabel + '</span></div>');
    parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
    $(this).find('option').each(function () {
      if ($(this).val() != "") {
        parent.find("ul.dropdown-list").append('<li id="' + $(this).val() + '">' + $(this).text() + '</li>');
      }
    });
  });

  $wrapper.find('.dropdown_select.input .dropdown-header').click(function (event) {
    $(this).toggleClass('slide-down').siblings('.dropdown-list').fadeToggle();
    $(this).children('.arrow-white').toggle();
  });

  $wrapper.find('.dropdown-list li').click(function () {
    var choose = $(this).text();
    var chooseVal = $(this).attr('id');
    $(this).parent().siblings('.dropdown-header').find('span').removeAttr('class').text(choose);
    $(this).parent().siblings('select').find('option').removeAttr('selected');
    $(this).parent().siblings('select').find('option[value="' + chooseVal + '"] ').attr('selected', 'selected').change();
    $(this).parent().find('li').removeClass('selected');
    $(this).addClass('selected');
    $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').fadeToggle();
    $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();

    smartSelect($(this));
  });
  $wrapper.find('.dropdown_select .input').click(function (event) {
    event.stopPropagation();
  });
}

function smartSelect(elem) {
  var dynamicDropdown = elem.parents('.hs-form-field').siblings('.hs-form-field');
  if (dynamicDropdown.length == 1 && dynamicDropdown.hasClass('rendered') == false) {
    dynamicDropdown.addClass('rendered');
    var parent2 = dynamicDropdown.find("select").parent();
    var firstLabel = dynamicDropdown.find('option').first().html();
    parent2.addClass('dropdown_select');
    parent2.append('<div class="dropdown-header"><span>' + firstLabel + '</span></div>');
    parent2.append('<ul class="dropdown-list" style="display: none;"></ul>');
    dynamicDropdown.find('option').each(function () {
      if ($(this).val() != "") {
        parent2.find("ul.dropdown-list").append('<li id="' + $(this).val() + '">' + $(this).val() + '</li>');
      }
    });

    dynamicDropdown.find('.dropdown-header').click(function (event) {
      $(this).toggleClass('slide-down').siblings('.dropdown-list').fadeToggle();
      $(this).children('.arrow-white').toggle();
    });

    dynamicDropdown.find('.dropdown-list li').click(function () {
      var choose = $(this).text();
      var chooseVal = $(this).attr('id');
      $(this).parent().siblings('.dropdown-header').find('span').text(choose);
      $(this).parent().siblings('select').find('option').removeAttr('selected');
      $(this).parent().siblings('select').val(choose).find(' option[value="' + chooseVal + '"] ').attr('selected', 'selected').change();
      $(this).parent().find('li').removeClass('selected');
      $(this).addClass('selected');
      $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
      $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    dynamicDropdown.find('.dropdown_select .input').click(function (event) {
      event.stopPropagation();
    });
  }
}
// end file: forms.js

// file: global.js
function waitForLoad(wrapper, element, callback) {
  if ($(wrapper).length > 0) {
    $(wrapper).each(function (i, el) {
      var waitForLoad = setInterval(function () {
        if ($(el).length == $(el).find(element).length) {
          clearInterval(waitForLoad);
          callback($(el), $(el).find(element));
        }
      }, 50);
    });
  }
}
// end file: global.js

// file: header.js
$(document).ready(function () {

  smallerHeaderOnScroll();
  initLangSwitcher();

  $('.hi-language-switcher-active').on('click', function () {
    $(this).parents('.hi-language-switcher').toggleClass('expand');
  });

  $('.search-button').on('click', function () {
    $(this).parents('.hi-search').toggleClass('expand');
    setTimeout(function () {
      $('.hi-search .hs-search-field-nav__input').focus();
    }, 300);
  });

  $('.search-bar-close').on('click', function () {
    $(this).parents('.hi-search').toggleClass('expand');
    $('.hi-search .hs-search-field-nav__input').blur();
  });

  $('.hi-menu-button .hamburger-button').on('click', function () {
    $('.hi-main-menu').addClass('expand');
    $('body').addClass('blur');
  });

  $('.hi-main-menu .menu-close-button').on('click', function () {
    $('.hi-main-menu').removeClass('expand');
    $('body').removeClass('blur');
  });
});

function smallerHeaderOnScroll() {
  var scrollTop = 0;
  $(window).scroll(function () {
    scrollTop = $(window).scrollTop();

    if (scrollTop >= 160) {
      $('.hi-header').addClass('hi-header--small');
    } else if (scrollTop < 160) {
      $('.hi-header').removeClass('hi-header--small');
    }
  });
}

$(document).on('keydown', function (e) {
  if (e.keyCode === 27) {
    // ESC
    $('body').removeClass('blur blur--strong');
    $('.hi-main-menu').removeClass('expand');
  }
});

function initLangSwitcher() {
  var findLang = "";
  var pasteHref = "";
  if ($('#hs_cos_wrapper_language_switcher .lang_list_class li').length > 0) {
    $('#hs_cos_wrapper_language_switcher .lang_list_class li a').each(function () {
      findLang = $(this).data('language');
      pasteHref = $(this).attr('href');
      $('.hi-language-switcher').find('a[data-language=' + findLang + ']').attr('href', pasteHref);
    });
  }
}
// end file: header.js

// file: modal.js
$(document).ready(function () {
  $('.hi-modal').each(function () {
    $('body').prepend($(this));
  });
});

$('.hi-modal-button').on('click', function () {
  var relModal = $('#' + $(this).attr('rel'));
  $('body').addClass('blur blur--strong');
  relModal.addClass('in');
});

$('.hi-modal-close').on('click', function () {
  $('body').removeClass('blur blur--strong');
  $(this).parents('.hi-modal').removeClass('in');
});

$(document).on('keydown', function (e) {
  if (e.keyCode === 27) {
    // ESC
    $('body').removeClass('blur blur--strong');
    $('.hi-modal').removeClass('in');
  }
});
// end file: modal.js

// file: Blog/hi-blog.js
$(document).ready(function () {
  blogSwitcher();
  popRelPostsSwitcher();
});

function blogSwitcher() {

  var blogSwitcher = $('.hi-blog-sidebar__blog-switcher').clone();
  $('#mobile-placeholder[rel="blog-switcher-placeholder"').append(blogSwitcher);

  $('.hi-blog-sidebar__blog-switcher li:first-of-type').hover(function () {
    $(this).parents('ul').addClass('left-hovered');
  }, function () {
    $(this).parents('ul').removeClass('left-hovered');
  });
  $('.hi-blog-sidebar__blog-switcher li:last-of-type').hover(function () {
    $(this).parents('ul').addClass('right-hovered');
  }, function () {
    $(this).parents('ul').removeClass('right-hovered');
  });

  if ($('.hi-blog-sidebar__blog-switcher li:first-of-type').hasClass('active')) {
    $('.hi-blog-sidebar__blog-switcher ul').addClass('left-active');
  } else if ($('.hi-blog-sidebar__blog-switcher li:last-of-type').hasClass('active')) {
    $('.hi-blog-sidebar__blog-switcher ul').addClass('right-active');
  }
}

function popRelPostsSwitcher() {
  $('.hi-blog-sidebar__other-posts .recent-posts-button').on('click', function () {
    $('.hi-blog-sidebar__other-posts .other-posts-wrapper ul li').removeClass('active');
    $(this).parents('li').addClass('active');
    $('.hi-blog-sidebar__other-posts .posts-tab').removeClass('active');
    $('.hi-blog-sidebar__other-posts .posts-tab[rel="recent-posts"]').addClass('active');
  });

  $('.hi-blog-sidebar__other-posts .popular-posts-button').on('click', function () {
    $('.hi-blog-sidebar__other-posts .other-posts-wrapper ul li').removeClass('active');
    $(this).parents('li').addClass('active');
    $('.hi-blog-sidebar__other-posts .posts-tab').removeClass('active');
    $('.hi-blog-sidebar__other-posts .posts-tab[rel="popular-posts"]').addClass('active');
  });
}
// end file: Blog/hi-blog.js
//# sourceMappingURL=template.js.map
