$(document).ready(function() {
	blogSwitcher();
	popRelPostsSwitcher();
})

function blogSwitcher() {

	var blogSwitcher = $('.hi-blog-sidebar__blog-switcher').clone();
	$('#mobile-placeholder[rel="blog-switcher-placeholder"').append(blogSwitcher);

	$('.hi-blog-sidebar__blog-switcher li:first-of-type').hover(function() {
		$(this).parents('ul').addClass('left-hovered');
	}, function() {
		$(this).parents('ul').removeClass('left-hovered');
	});
	$('.hi-blog-sidebar__blog-switcher li:last-of-type').hover(function() {
		$(this).parents('ul').addClass('right-hovered');
	}, function() {
		$(this).parents('ul').removeClass('right-hovered');
	});

	if ($('.hi-blog-sidebar__blog-switcher li:first-of-type').hasClass('active')) {
		$('.hi-blog-sidebar__blog-switcher ul').addClass('left-active');
	} else if ($('.hi-blog-sidebar__blog-switcher li:last-of-type').hasClass('active')) {
		$('.hi-blog-sidebar__blog-switcher ul').addClass('right-active');
	}
}

function popRelPostsSwitcher() {
	$('.hi-blog-sidebar__other-posts .recent-posts-button').on('click', function() {
		$('.hi-blog-sidebar__other-posts .other-posts-wrapper ul li').removeClass('active');
		$(this).parents('li').addClass('active');
		$('.hi-blog-sidebar__other-posts .posts-tab').removeClass('active');
		$('.hi-blog-sidebar__other-posts .posts-tab[rel="recent-posts"]').addClass('active');
	});

	$('.hi-blog-sidebar__other-posts .popular-posts-button').on('click', function() {
		$('.hi-blog-sidebar__other-posts .other-posts-wrapper ul li').removeClass('active');
		$(this).parents('li').addClass('active');
		$('.hi-blog-sidebar__other-posts .posts-tab').removeClass('active');
		$('.hi-blog-sidebar__other-posts .posts-tab[rel="popular-posts"]').addClass('active');
	})
}