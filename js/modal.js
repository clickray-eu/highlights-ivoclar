$(document).ready(function() {
	$('.hi-modal').each(function () {
		$('body').prepend($(this));
	})
})

$('.hi-modal-button').on('click', function() {
	var relModal = $('#' + $(this).attr('rel'));
	$('body').addClass('blur blur--strong');
	relModal.addClass('in');
})

$('.hi-modal-close').on('click', function() {
	$('body').removeClass('blur blur--strong');
	$(this).parents('.hi-modal').removeClass('in');
})

$(document).on('keydown', function (e) {
    if (e.keyCode === 27) { // ESC
        $('body').removeClass('blur blur--strong');
		$('.hi-modal').removeClass('in');
    }
});