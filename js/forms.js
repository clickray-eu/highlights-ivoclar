// waitForLoad(".form, .widget-type-form, .widget-type-blog_content", "form", select);
// waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", function() {
//     $('form select').on('change', function() {
//         $('form .dropdown-header').addClass('dropdown-selected');
//     });
// });

// Functions for dropdown
function select($wrapper,$form) {
    $wrapper.find("select").each(function () {
        var parent = $(this).parent();
        var firstLabel = $(this).find('option').first().html();
        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span class="first-label">'+ firstLabel +'</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        $(this).find('option').each(function () {
            if ($(this).val() != "") {
                parent.find("ul.dropdown-list").append('<li id="' + $(this).val() + '">' + $(this).text() + '</li>');
            }
        });
    });

    $wrapper.find('.dropdown_select.input .dropdown-header').click(function (event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').fadeToggle();
        $(this).children('.arrow-white').toggle();
    });

    $wrapper.find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        var chooseVal = $(this).attr('id');
        $(this).parent().siblings('.dropdown-header').find('span').removeAttr('class').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').find('option[value="' + chooseVal + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').fadeToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
        
        smartSelect($(this));
        
    });
    $wrapper.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

function smartSelect(elem) {
    var dynamicDropdown = elem.parents('.hs-form-field').siblings('.hs-form-field');
    if (dynamicDropdown.length == 1 && dynamicDropdown.hasClass('rendered') == false) {
        dynamicDropdown.addClass('rendered');
        var parent2 = dynamicDropdown.find("select").parent();
        var firstLabel = dynamicDropdown.find('option').first().html();
        parent2.addClass('dropdown_select');
        parent2.append('<div class="dropdown-header"><span>'+ firstLabel +'</span></div>');
        parent2.append('<ul class="dropdown-list" style="display: none;"></ul>');
        dynamicDropdown.find('option').each(function () {
            if ($(this).val() != "") {
                parent2.find("ul.dropdown-list").append('<li id="' + $(this).val() + '">' + $(this).val() + '</li>');
            }
        });
        
        dynamicDropdown.find('.dropdown-header').click(function (event) {
            $(this).toggleClass('slide-down').siblings('.dropdown-list').fadeToggle();
            $(this).children('.arrow-white').toggle();
        });
        
        dynamicDropdown.find('.dropdown-list li').click(function () {
            var choose = $(this).text();
            var chooseVal = $(this).attr('id');
            $(this).parent().siblings('.dropdown-header').find('span').text(choose);
            $(this).parent().siblings('select').find('option').removeAttr('selected');
            $(this).parent().siblings('select').val(choose).find(' option[value="' + chooseVal + '"] ').attr('selected', 'selected').change();
            $(this).parent().find('li').removeClass('selected');
            $(this).addClass('selected');
            $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
            $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
            
        });
        dynamicDropdown.find('.dropdown_select .input').click(function (event) {
            event.stopPropagation();
        });
    }
}