$(document).ready(function() {

	smallerHeaderOnScroll();
	initLangSwitcher();

	$('.hi-language-switcher-active').on('click', function() {
		$(this).parents('.hi-language-switcher').toggleClass('expand');
	});

	$('.search-button').on('click', function() {
		$(this).parents('.hi-search').toggleClass('expand');
		setTimeout(function() {
			$('.hi-search .hs-search-field-nav__input').focus();
		}, 300);
	});

	$('.search-bar-close').on('click', function() {
		$(this).parents('.hi-search').toggleClass('expand');
		$('.hi-search .hs-search-field-nav__input').blur();
	});

	$('.hi-menu-button .hamburger-button').on('click', function() {
		$('.hi-main-menu').addClass('expand');
		$('body').addClass('blur')
	});

	$('.hi-main-menu .menu-close-button').on('click', function() {
		$('.hi-main-menu').removeClass('expand');
		$('body').removeClass('blur')
	});

});


function smallerHeaderOnScroll() {
  var scrollTop = 0;
  $(window).scroll(function(){
    scrollTop = $(window).scrollTop();
    
    if (scrollTop >= 160) {
      $('.hi-header').addClass('hi-header--small');
    } else if (scrollTop < 160) {
      $('.hi-header').removeClass('hi-header--small');
    } 
    
  }); 
}

$(document).on('keydown', function (e) {
    if (e.keyCode === 27) { // ESC
        $('body').removeClass('blur blur--strong');
		$('.hi-main-menu').removeClass('expand');
    }
});

function initLangSwitcher() {
	var findLang = "";
	var pasteHref = "";
	if ($('#hs_cos_wrapper_language_switcher .lang_list_class li').length > 0) {
		$('#hs_cos_wrapper_language_switcher .lang_list_class li a').each(function() {
			findLang = $(this).data('language');
			pasteHref = $(this).attr('href');
			$('.hi-language-switcher').find('a[data-language=' + findLang + ']').attr('href', pasteHref);
		})
	}
}